function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
      <div class="card shadow mb-3 ">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
          <p class="card-text">${startDate} - ${endDate}</p>
        </div>
      </div>
    `;
}


function placeholderCard() {
  return `
    <div class="card" aria-hidden="true">
      <img src="..." class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title placeholder-glow">
          <span class="placeholder col-6"></span>
        </h5>
        <p class="card-text placeholder-glow">
          <span class="placeholder col-7"></span>
          <span class="placeholder col-4"></span>
          <span class="placeholder col-4"></span>
          <span class="placeholder col-6"></span>
          <span class="placeholder col-8"></span>
        </p>
        <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
      </div>
    </div>
  `;
}


window.addEventListener('DOMContentLoaded', async() => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            throw new Error('Network response was not ok');
        } else {
            const data = await response.json();
            let index = 0;

            for (let conference of data.conferences) {

                const column = document.querySelector(`#col-${index % 3}`);
                const placeholder = placeholderCard();
                const tempHTML = column.innerHTML;
                column.innerHTML += placeholder;

                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;

                    let startDateC = new Date(details.conference.starts);
                    startDateC = startDateC.toLocaleDateString();

                    let endDateC = new Date(details.conference.ends);
                    endDateC = endDateC.toLocaleDateString();

                    const location = details.conference.location.name;

                    const html = createCard(title, description, pictureUrl, startDateC, endDateC, location);
                    //const column = document.querySelector(`#col-${index % 3}`);

                    column.innerHTML = tempHTML;

                    column.innerHTML += html;
                    index++;
                }
            }

            // const conference = data.conferences[0];
            // const nameTag = document.querySelector('.card-title');
            // nameTag.innerHTML = conference.name;

            // const detailUrl = `http://localhost:8000${conference.href}`;
            // const detailResponse = await fetch(detailUrl);

            // if (detailResponse.ok) {
            //     const details = await detailResponse.json();

            //     const conferenceDetail = details.conference;
            //     const descTag = document.querySelector('.card-text');
            //     descTag.innerHTML = conferenceDetail.description;

            //     const imageTag = document.querySelector('.card-img-top');
            //     imageTag.src = details.conference.location.picture_url;
            // }
        }
    } catch (e) {
        console.error(e);
    }
});
